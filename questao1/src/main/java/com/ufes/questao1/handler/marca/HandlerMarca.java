/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao1.handler.marca;

/**
 *
 * @author bruno
 */
public abstract class HandlerMarca {
    
    protected HandlerMarca successor;
    
    public void setSucessor(HandlerMarca successor) {
        this.successor = successor;
    }
    
    public String goToNext(String texto) {
        if(successor != null) {
            texto = successor.handleRequest(texto);
        }
        return texto;
    }
     
    public abstract String handleRequest(String message);

}
