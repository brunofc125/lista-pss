/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao1.handler.marca;

import com.ufes.questao1.singleton.StringUtil;

/**
 *
 * @author bruno
 */
public class IBMHandler extends HandlerMarca {

    @Override
    public String handleRequest(String message) {
        StringUtil stringUtil = StringUtil.getInstance();
        String texto = stringUtil.substitui(message, "ibm", '*');
        return goToNext(texto);
    }

}
