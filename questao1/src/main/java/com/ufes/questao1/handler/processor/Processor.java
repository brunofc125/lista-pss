/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao1.handler.processor;

/**
 *
 * @author bruno
 */
public interface Processor {
    public String handleRequest(String message);
}
