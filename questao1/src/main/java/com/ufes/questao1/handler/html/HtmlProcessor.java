/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao1.handler.html;

import com.ufes.questao1.handler.processor.Processor;

/**
 *
 * @author bruno
 */
public class HtmlProcessor implements Processor {
    
    protected HandlerHtml last;
    protected HandlerHtml first;
    
    public HtmlProcessor(HandlerHtml handlerHtml) {
        if(handlerHtml == null) {
            throw new RuntimeException("Handler não informado");
        }
        this.first = handlerHtml; 
        this.last = handlerHtml;
    }
    
    public void addHandlerHtml(HandlerHtml handlerHtml) {        
        if(handlerHtml == null) {
            throw new RuntimeException("Handler não informado");
        }
        this.last.setSucessor(handlerHtml);
        this.last = handlerHtml;
    }
    
    @Override
    public String handleRequest(String message) {
        if(message == null || message.isBlank()){
            throw new RuntimeException("Messagem não informada");
        }
        return this.first.handleRequest(message);
    }
    
    
}
