/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao1.handler.html;

import com.ufes.questao1.singleton.StringUtil;

/**
 *
 * @author bruno
 */
public class TableHandler extends HandlerHtml {

    @Override
    public String handleRequest(String message) {
        StringUtil stringUtil = StringUtil.getInstance();
        if(stringUtil.containsStr(message, "<table")) {
            return "Mensagem removida por conter conteúdo não autorizado";
        }
        return goToNext(message);
    }

}
