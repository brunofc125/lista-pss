/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao1.handler.marca;

import com.ufes.questao1.handler.processor.Processor;

/**
 *
 * @author bruno
 */
public class MarcaProcessor implements Processor {
    protected HandlerMarca last;
    protected HandlerMarca first;
    
    public MarcaProcessor(HandlerMarca handlerMarca) {
        if(handlerMarca == null) {
            throw new RuntimeException("Handler não informado");
        }
        this.first = handlerMarca; 
        this.last = handlerMarca;
    }
    
    public void addHandlerMarca(HandlerMarca handlerMarca) {        
        if(handlerMarca == null) {
            throw new RuntimeException("Handler não informado");
        }
        this.last.setSucessor(handlerMarca);
        this.last = handlerMarca;
    }
    
    @Override
    public String handleRequest(String message) {
        if(message == null || message.isBlank()){
            throw new RuntimeException("Messagem não informada");
        }
        return this.first.handleRequest(message);
    }

    
    
}
