/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao1.proxy;

import com.ufes.questao1.chat.MediatorChat;
import com.ufes.questao1.chat.Participante;
import com.ufes.questao1.handler.processor.Processor;

/**
 *
 * @author bruno
 */
public class SalaChatProxy implements MediatorChat {

    private final MediatorChat chat;
    private Processor processor;

    public SalaChatProxy(MediatorChat chat, Processor processor) {
        if(chat == null) {
            throw new RuntimeException("Chat não informado");
        }
        this.chat = chat;
        this.processor = processor;
    }
    
    public SalaChatProxy(MediatorChat chat) {
        if(chat == null) {
            throw new RuntimeException("Chat não informado");
        }
        this.chat = chat;
    }

    @Override
    public void enviar(Participante participante, String message) {
        if(processor != null) {
            message = processor.handleRequest(message);
        }
        chat.enviar(participante, message);
    }

    @Override
    public Participante criarParticipante(MediatorChat mediator, String name) {
        return chat.criarParticipante(mediator, name);
    }
    
    public void setProcessor(Processor processor) {
        this.processor = processor;
    }
    
}
