/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao1;

import com.ufes.questao1.builder.SalaChatHtmlBuilder;
import com.ufes.questao1.builder.SalaChatMarcaBuilder;
import com.ufes.questao1.builder.diretor.SalaChatDiretor;
import com.ufes.questao1.chat.MediatorChat;
import com.ufes.questao1.chat.Participante;

/**
 *
 * @author bruno
 */
public class Principal {
    public static void main(String[] args) {
        SalaChatDiretor diretor = new SalaChatDiretor();
        MediatorChat chatMarca = diretor.build(new SalaChatMarcaBuilder());
        MediatorChat chatHtml = diretor.build(new SalaChatHtmlBuilder());
        
        System.out.println("CHAT - NÃO MARCAS ");
        Participante participante1 = chatMarca.criarParticipante(chatMarca, "Bruno");
        Participante participante2 = chatMarca.criarParticipante(chatMarca, "Joao");
        participante1.enviar("Apple, Microsoft e IBM são as marcas mais valiosas do mundo");
        participante2.enviar("Não Entendi");
        
        System.out.println("\nCHAT - NÃO PODE TAGS HTML ");
        Participante participante3 = chatHtml.criarParticipante(chatHtml, "Claudio");
        Participante participante4 = chatHtml.criarParticipante(chatHtml, "Eduardo");
        participante3.enviar("Tag1: <a href ---");
        participante3.enviar("Teste1");
        participante4.enviar("Tag1: <img ---");
        participante4.enviar("Teste2");
        participante3.enviar("Tag1: <p ---");
        participante3.enviar("Teste3");
        participante4.enviar("Tag1: <table ---");
        participante4.enviar("Teste4");
        

        
    }
}
