/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao1.builder.diretor;

import com.ufes.questao1.builder.MediatorChatBuilder;
import com.ufes.questao1.chat.MediatorChat;

/**
 *
 * @author bruno
 */
public class SalaChatDiretor {
    public MediatorChat build(MediatorChatBuilder builder) {
        builder.build();
        builder.putIntercept();
        return builder.getMediatorChat();
    }
}
