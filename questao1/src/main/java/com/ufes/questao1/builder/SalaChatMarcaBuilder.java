/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao1.builder;

import com.ufes.questao1.chat.SalaChat;
import com.ufes.questao1.handler.marca.AppleHandler;
import com.ufes.questao1.handler.marca.IBMHandler;
import com.ufes.questao1.handler.marca.MarcaProcessor;
import com.ufes.questao1.handler.marca.MicrosoftHandler;
import com.ufes.questao1.proxy.SalaChatProxy;

/**
 *
 * @author bruno
 */
public class SalaChatMarcaBuilder extends MediatorChatBuilder {

    @Override
    public void build() {
        setMediatorChat(new SalaChatProxy(new SalaChat()));
    }

    @Override
    public void putIntercept() {
        MarcaProcessor processor = new MarcaProcessor(new AppleHandler());
        processor.addHandlerMarca(new IBMHandler());
        processor.addHandlerMarca(new MicrosoftHandler());
        ((SalaChatProxy)getMediatorChat()).setProcessor(processor);        
    }
    
}
