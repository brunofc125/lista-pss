/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao1.builder;

import com.ufes.questao1.chat.MediatorChat;

/**
 *
 * @author bruno
 */
public abstract class MediatorChatBuilder {
    
    private MediatorChat mediatorChat;
    
    public abstract void build();
    public abstract void putIntercept();
    
    public final MediatorChat getMediatorChat() {
        return mediatorChat;
    }
    
    public void setMediatorChat(MediatorChat mediatorChat) {
        if(mediatorChat == null) {
            throw new RuntimeException("Mediator não informado");
        }
        this.mediatorChat = mediatorChat;
    }
    
}
