/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao1.builder;

import com.ufes.questao1.chat.SalaChat;
import com.ufes.questao1.handler.html.HrefHandler;
import com.ufes.questao1.handler.html.HtmlProcessor;
import com.ufes.questao1.handler.html.ImgHandler;
import com.ufes.questao1.handler.html.PHandler;
import com.ufes.questao1.handler.html.TableHandler;
import com.ufes.questao1.proxy.SalaChatProxy;

/**
 *
 * @author bruno
 */
public class SalaChatHtmlBuilder extends MediatorChatBuilder {

    @Override
    public void build() {
        setMediatorChat(new SalaChatProxy(new SalaChat()));
    }

    @Override
    public void putIntercept() {
        HtmlProcessor processor = new HtmlProcessor(new HrefHandler());
        processor.addHandlerHtml(new ImgHandler());
        processor.addHandlerHtml(new PHandler());
        processor.addHandlerHtml(new TableHandler());
        ((SalaChatProxy)getMediatorChat()).setProcessor(processor);        
    }
    
}
