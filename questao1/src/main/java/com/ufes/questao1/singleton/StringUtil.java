/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao1.singleton;

/**
 *
 * @author bruno
 */
public class StringUtil {

    private static StringUtil instance = null;

    private StringUtil() {

    }

    public static StringUtil getInstance() {
        if (instance == null) {
            instance = new StringUtil();
        }
        return instance;
    }

    public String substitui(String texto, String palavra, char simbolo) {
        if (texto == null || palavra == null) {
            return texto;
        }
        char[] textoClonado = texto.toCharArray();
        char[] textoMinusculo = texto.toLowerCase().toCharArray();
        String palavraMinuscula = palavra.toLowerCase();
        int j = 0;
        for (int i = 0; i < textoClonado.length; i++) {
            if (textoMinusculo[i] == palavraMinuscula.charAt(j)) {
                j++;
                if (j == palavra.length()) {
                    for (int l = i - j + 1; l <= i; l++) {
                        textoClonado[l] = simbolo;
                    }
                    j = 0;
                }
            } else {
                j = 0;
            }
        }
        return new String(textoClonado);
    }
    
    public boolean containsStr(String texto, String palavra) {
        if(texto == null || palavra == null || texto.isBlank() || palavra.isBlank()) {
            return false;
        }
        return texto.toLowerCase().contains(palavra.toLowerCase());
    }

}
