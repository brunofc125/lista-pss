/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao1.chat;

/**
 *
 * @author bruno
 */
public interface MediatorChat {
    public abstract void enviar(Participante participante, String mensagem);
    public abstract Participante criarParticipante(MediatorChat mediator, String name);
}
