/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao1.chat;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author bruno
 */
public class SalaChat implements MediatorChat {
    private List<Participante> participantes;
    
    public SalaChat() {
        this.participantes = new ArrayList<>();
    }

    @Override
    public void enviar(Participante participante, String mensagem) {
        participantes.forEach(p -> {
            if(!p.equals(participante)) {
                p.receber(mensagem, participante);
            }        
        });
    }

    @Override
    public Participante criarParticipante(MediatorChat mediator, String name) {
        Participante participante = new ParticipanteChat(mediator, name);
        this.participantes.add(participante);
        return participante;
    }
}
