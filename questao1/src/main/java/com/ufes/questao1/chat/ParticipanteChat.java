/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao1.chat;

/**
 *
 * @author bruno
 */
public class ParticipanteChat extends Participante {
    private String name;

    public ParticipanteChat(MediatorChat mediatorChat, String name) {
        this.mediator = mediatorChat;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void enviar(String message) {
        this.mediator.enviar(this, message);
    }

    @Override
    public void receber(String message, Participante participante) {
        System.out.println(participante.getName() + ": " + message);
    }
}
