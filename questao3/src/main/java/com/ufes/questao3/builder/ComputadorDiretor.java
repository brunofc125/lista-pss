/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao3.builder;

import com.ufes.questao3.composite.Computador;

/**
 *
 * @author bruno
 */
public class ComputadorDiretor {
    public Computador build(ComputadorBuilder builder) {
        builder.buildPC();
        return builder.getComputador();
    }
}
