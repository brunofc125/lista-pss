/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao3.builder;

import com.ufes.questao3.composite.Processador;
import com.ufes.questao3.composite.PlacaMae;
import com.ufes.questao3.composite.Hd;
import com.ufes.questao3.composite.Memoria;
import com.ufes.questao3.composite.Computador;

/**
 *
 * @author bruno
 */
public class ComputadorPadraoBuilder extends ComputadorBuilder {

    @Override
    public void buildPC() {
        this.setComputador(new Computador(1000, "Computador"));
        this.getComputador().add(new Hd(180, "HD 1TB"));
        this.getComputador().add(new PlacaMae(400, "Placa Mãe GIGABYTE"));
        this.getComputador().add(new Memoria(200, "Memória RAM 8gb"));
        this.getComputador().add(new Processador(1000, "I7"));
    }
    
}
