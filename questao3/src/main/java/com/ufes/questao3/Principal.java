/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao3;

import com.ufes.questao3.builder.ComputadorPadraoBuilder;
import com.ufes.questao3.builder.ComputadorDiretor;
import com.ufes.questao3.composite.Computador;

/**
 *
 * @author bruno
 */
public class Principal {
    public static void main(String[] args) {
        ComputadorDiretor diretor = new ComputadorDiretor();
        Computador c = diretor.build(new ComputadorPadraoBuilder());
        System.out.println(c.getDescricao());
        System.out.println(c.getPreco());
    }
}
