/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao3.composite;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author bruno
 */
public abstract class Todo extends Component {
    private List<Component> elementos = new ArrayList<>();

    public Todo(double preco, String descricao) {
        super(preco, descricao);
    }

    @Override
    public double getPreco() {
        double precoTotal = this.preco;
        for(Component e : this.elementos) {
            precoTotal += e.getPreco();
        }
        return precoTotal;
    }

    @Override
    public String getDescricao() {
        String descricaoCompleta = this.descricao;
        for(Component e : this.elementos) {
            descricaoCompleta += ", " + e.getDescricao();
        }
        return descricaoCompleta;
    }
    
    public void add(Component elemento) {
        this.elementos.add(elemento);
    }
    
    
}
