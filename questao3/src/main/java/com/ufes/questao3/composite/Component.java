/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao3.composite;

/**
 *
 * @author bruno
 */
public abstract class Component {
    protected double preco;
    protected String descricao;
    
    public Component(double preco, String descricao) {
        if(preco < 0) {
            throw new RuntimeException("Valor de preço inválido");
        }
        if (descricao == null || descricao.isBlank()) {
            throw new RuntimeException("Descrição não informado");
        }
        this.preco = preco;
        this.descricao = descricao;
    }

    public abstract double getPreco();
    public abstract String getDescricao();
    
}
