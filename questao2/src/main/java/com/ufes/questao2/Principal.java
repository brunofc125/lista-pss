/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao2;

import com.ufes.questao2.decorator.Elemento;
import com.ufes.questao2.decorator.Ingrediente;
import com.ufes.questao2.diretor.SanduicheDiretor;
import com.ufes.questao2.builder.BeiruteBuilder;
import com.ufes.questao2.builder.CapreseBuilder;

/**
 *
 * @author bruno
 */
public class Principal {
    public static void main(String[] args) {
        SanduicheDiretor d = new SanduicheDiretor();

        Elemento e = d.build(new BeiruteBuilder());
        e = new Ingrediente(e, 1, "Manjericão");
        
        System.out.println("Descrição: " + e.getDescricao());
        System.out.println("Preço: " + e.getPreco());
        
        Elemento e2 = d.build(new CapreseBuilder());
        e2 = new Ingrediente(e2, 1, "Ovo frito");
        
        System.out.println("Descrição: " + e2.getDescricao());
        System.out.println("Preço: " + e2.getPreco());
        
    }
}
