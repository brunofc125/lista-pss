/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao2.diretor;

import com.ufes.questao2.decorator.Elemento;
import com.ufes.questao2.builder.SanduicheBuilder;

/**
 *
 * @author bruno
 */
public class SanduicheDiretor {
    
    public Elemento build(SanduicheBuilder builder) {
        builder.baseSanduiche();
        return builder.getSanduiche();
    }
}
