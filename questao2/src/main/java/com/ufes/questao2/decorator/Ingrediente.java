/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao2.decorator;

/**
 *
 * @author bruno
 */
public class Ingrediente extends DecoratorSanduiche {
    
    public Ingrediente(Elemento elemento, double preco, String descricao) {
        super(elemento, preco, descricao);
    }

    @Override
    public double getPreco() {
        return elementoDecorado.getPreco() + preco;
    }

    @Override
    public String getDescricao() {
        return elementoDecorado.getDescricao() + ", " + descricao;
    }
    
    
    
}
