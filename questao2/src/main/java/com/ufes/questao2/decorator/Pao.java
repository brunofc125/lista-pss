/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao2.decorator;

/**
 *
 * @author bruno
 */
public class Pao extends Elemento {

    public Pao(double preco, String descricao) {
        super(preco, descricao);
    }

    @Override
    public double getPreco() {
        return preco;
    }

    @Override
    public String getDescricao() {
        return descricao;
    }
    
}
