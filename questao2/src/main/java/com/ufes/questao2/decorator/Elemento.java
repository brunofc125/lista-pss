/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao2.decorator;

/**
 *
 * @author bruno
 */
public abstract class Elemento {
    protected double preco;
    protected String descricao;
    
    public Elemento(double preco, String descricao) {
        this.preco = preco;
        this.descricao = descricao;
    }

    public abstract double getPreco();

    public  abstract String getDescricao();
    
}
