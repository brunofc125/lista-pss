/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao2.decorator;

/**
 *
 * @author bruno
 */
public abstract class DecoratorSanduiche extends Elemento {
    
    protected Elemento elementoDecorado;
    
    public DecoratorSanduiche(Elemento elemento, double preco, String descricao) {
        super(preco, descricao);
        if(elemento == null) {
           throw new RuntimeException("Elemento não informado");
        }
        this.elementoDecorado = elemento;
    }
    
}
