/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao2.builder;

import com.ufes.questao2.decorator.Elemento;

/**
 *
 * @author bruno
 */
public abstract class SanduicheBuilder {
    private Elemento sanduiche;
    
    public abstract void baseSanduiche();
    
    public final Elemento getSanduiche() {
        return sanduiche;
    }
    
    public void setSanduiche(Elemento sanduiche) {
        this.sanduiche = sanduiche;
    }
}
