/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufes.questao2.builder;

import com.ufes.questao2.decorator.Ingrediente;
import com.ufes.questao2.decorator.Pao;

/**
 *
 * @author bruno
 */
public class CapreseBuilder extends SanduicheBuilder {

    @Override
    public void baseSanduiche() {
        setSanduiche(new Pao(1, "Sanduíche de Focaccia"));
        setSanduiche(new Ingrediente(getSanduiche(), 1.5, "mozzarella"));
        setSanduiche(new Ingrediente(getSanduiche(), 0.5, "manjericão"));
        setSanduiche(new Ingrediente(getSanduiche(), 1, "Tomate"));  
    }
    
}
